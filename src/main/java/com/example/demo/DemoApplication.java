package com.example.demo;

//import com.mongodb.MongoClient;
//import com.example.demo.test.TT;
//import com.example.demo.test.TTRepo;
//import com.example.demo.test.TT;
//import com.example.demo.test.TTRepo;
import net.javacrumbs.shedlock.core.LockProvider;
import net.javacrumbs.shedlock.core.SchedulerLock;
//import net.javacrumbs.shedlock.provider.mongo.MongoLockProvider;
import net.javacrumbs.shedlock.spring.annotation.EnableSchedulerLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;


@SpringBootApplication
@EnableScheduling
@EnableSchedulerLock(defaultLockAtMostFor = "PT30S")
public class DemoApplication implements CommandLineRunner {

//	@Autowired
//	EmailServiceImpl emailService;

//	@Autowired
//	TaskScheduler taskScheduler;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

//	@Autowired
//	TTRepo ttRepo;
	@Override
	public void run(String... args) throws Exception {
//		System.out.println("send");
//		emailService.sendSimpleMessage("dengbunthaitest@gmail.com", "AAA", "bbb");
//		ttRepo.save(new TT("aazzzaa"));
	}


}
